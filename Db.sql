/*
Para el uso con RAILS colocar los nombres de las tablas y llaves foráneas en plural
De acuerdo a la base de datos utilizada la sintaxis del AUTO_INCREMENT puede variar
http://www.w3schools.com/sql/sql_AUTO_INCREMENT.asp
Los tipos de dato también pueden varias entre bases de datos, se recomienda utilizar
campos ENUM o BOOLEAN para los campos bandera cuando la base de datos lo permita:
professor.is_admin, student_id_admin, group.enabled => boolean
test.status => ENUM('inactive','active','finished');
*/
CREATE TABLE Userstype (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	type varchar(255) NOT NULL,
	created_at datetime NOT NULL,
	updated_at datetime);

CREATE TABLE Users_has_type (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	userstype varchar(255) NOT NULL  REFERENCES Userstype(id) ON DELETE RESTRICT,
	id_user INTEGER NOT NULL REFERENCES Users(id) ON DELETE RESTRICT,
	created_at datetime NOT NULL,
	updated_at datetime);

CREATE TABLE Users (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	username varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	created_at datetime NOT NULL,
	updated_at datetime);

CREATE TABLE professor (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	document_number varchar(255) NOT NULL,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	created_at datetime NOT NULL,
	updated_at datetime);

CREATE TABLE administrative (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	document_number varchar(255) NOT NULL,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	created_at datetime NOT NULL,
	updated_at datetime);

CREATE TABLE classrooms (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	cod varchar(255) NOT NULL,
	name varchar(255) NOT NULL,
	location varchar(255) NOT NULL,
	created_at datetime NOT NULL,
	updated_at datetime);


CREATE TABLE student (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	document_number varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	created_at datetime NOT NULL,
	updated_at datetime);

CREATE TABLE course (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	code varchar(255) NOT NULL,
	name varchar(255) NOT NULL,
	description varchar(255),
	created_at datetime NOT NULL,
	updated_at datetime);

CREATE TABLE `group` (
	id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
	course_id integer NOT NULL REFERENCES course(id) ON DELETE RESTRICT,
	quarter varchar(255),
	professor_id integer NOT NULL REFERENCES professor(id) ON DELETE RESTRICT,
	group_number integer DEFAULT 0,
	created_at datetime NOT NULL,
	updated_at datetime,
	enabled integer DEFAULT 1);







