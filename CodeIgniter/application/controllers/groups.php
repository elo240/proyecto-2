<?php

class Groups extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
	}

	public function index() 
	{
		$this->load->model('groups_model'); //cargamos el modelo
 		$this->load->model('professors_model');
 		$this->load->model('courses_model');

		$data['page_title'] = "Tarea 1";
 		
		//Obtener datos de la tabla 
		$groups = $this->groups_model->getData(); //llamamos a la función getData() del modelo creado anteriormente.
		$tabledata = $this->professors_model->getData();
		$tabledata1 = $this->courses_model->getData();

		//Unir el first name y el last name obtenido en la consulta en el modelo. Dependiendo del Id del profesor.
   		foreach ($groups as $row)
   		{
       		$nombre = $row->first_name.' '.$row->last_name;
 	   		$row->first_name= $nombre;
 	  		unset($row->last_name);
  		}

 		$data['data'] = $groups;
 		$data['JS_CSS_files'] = JS_CSS_files();
 		$data['navbar'] = navbar();
 
 		$heading=   array('Nombre de curso','Cuatrimestre','Nombre profesor','Numero de grupo','Created at','Updated at','Activo','Borrar / Editar');

		$modal=  array(array('text' => 'Nombre curso','type' => 'datalist','tableName' => 'courses','table' => $tabledata1),
					   array('text' => 'Cuatrimestre','type' => 'text' ),
 					   array('text' => 'Nombre profesor','type' => 'datalist','tableName' => 'professor','table' => $tabledata),
 				 	   array('text' => 'Numero de grupo','type' => 'text' ));

		$seccion = 'Grupos';
 		$modalRoute = 'Grupos/Agregar';
 		$data['modal']= modal($modal,$modalRoute,$seccion);
 		$data['table']= _table($groups,$heading,$seccion);
 
 		//load de vistas
 		$this->load->view('cruds_view', $data); //llamada a la vista, que crearemos posteriormente
	}
	public function insert() 
	{
 		//recogemos los datos obtenidos por POST
		$data['course_id'] = $_POST['input1'];
		$data['quarter'] = $_POST['input2'];
		$data['professor_id'] = $_POST['input3'];
		$data['group_number'] = $_POST['input3'];
		$data['updated_at'] =  date('Y-m-d-G-i-s-A');
 		//llamamos al modelo, concretamente a la función insert() para que nos haga el insert en la base de datos.
		$this->load->model('groups_model');
		$this->groups_model->insert($data);
		//volvemos a visualizar la tabla
		redirect('/Grupos','refresh');
		$this->index();
	}
	public function delete() 
	{
		//obtenemos el nombre
		$id = $this->uri->segment(3);
		//cargamos el modelo y llamamos a la función baja(), pasándole el nombre del registro que queremos borrar.
		$this->load->model('groups_model');
		$this->groups_model->delete($id);
		//mostramos la vista de nuevo.
		redirect('/Grupos','refresh');
		$this->index();
	}
	
	public function update() 
	{
 		//recogemos los datos obtenidos por POST
		$data['course_id'] = $_POST['input1'];
		$data['quarter'] = $_POST['input2'];
		$data['professor_id'] = $_POST['input3'];
		$data['group_number'] = $_POST['input3'];
		$data['updated_at'] =  date('Y-m-d-G-i-s-A');
		$data['id'] = $this->uri->segment(3);
 		//llamamos al modelo, concretamente a la función insert() para que nos haga el insert en la base de datos.
		$this->load->model('groups_model');
		$this->groups_model->updated($data);
 		//volvemos a visualizar la tabla
		redirect('/Grupos','refresh');
		$this->index();
	}
}
?>