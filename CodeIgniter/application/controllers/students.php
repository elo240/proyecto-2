<?php

class Students extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index() 
	{
 		$this->load->model('students_model'); //cargamos el modelo
 
 		$data['page_title'] = "Tarea 1";
 
 		//Obtener datos de la tabla 'contacto'
 		$students = $this->students_model->getData(); //llamamos a la función getData() del modelo creado anteriormente.
 
 		$data['data'] = $students;
		$data['JS_CSS_files'] = JS_CSS_files();
 		$data['navbar'] = navbar();
 
 		$heading=   array('First name','Last name','Document number','Email','Created at','Updated at','Borrar / Editar');

 		$modal=   array(array('text' => 'First name','type' => 'text' ),
 						array('text' => 'Last name','type' => 'text' ),
 						array('text' => 'Document number','type' => 'text' ),
 						array('text' => 'Email','type' => 'email' ));

		$seccion = 'Estudiantes';
 		$modalRoute = 'Estudiantes/Agregar';
 		$data['modal']= modal($modal,$modalRoute,$seccion);
 		$data['table']= _table($students,$heading,$seccion);
 
		//load de vistas
 		$this->load->view('cruds_view', $data); //llamada a la vista, que crearemos posteriormente
	}
	public function insert()
	{
 		//recogemos los datos obtenidos por POST
		$data['first_name'] = $_POST['input1'];
		$data['last_name'] = $_POST['input2'];
		$data['document_number'] = $_POST['input3'];
		$data['email'] = $_POST['input4'];
		$data['created_at'] = date('Y-m-d-G-i-s-A');
		$data['updated_at'] = date('Y-m-d-G-i-s-A');
 		//llamamos al modelo, concretamente a la función insert() para que nos haga el insert en la base de datos.
		$this->load->model('students_model');
		$this->students_model->insert($data);
 		//volvemos a visualizar la tabla
		redirect('/Estudiantes','refresh');
		$this->index();
	}
	public function delete() 
	{
 		//obtenemos el nombre
		$id = $this->uri->segment(3);
 		//cargamos el modelo y llamamos a la función baja(), pasándole el nombre del registro que queremos borrar.
		$this->load->model('students_model');
		$this->students_model->delete($id);
 		//mostramos la vista de nuevo.
		redirect('/Estudiantes','refresh');
		$this->index();
	}
	public function editar() 
	{
 		//cargamos el modelo y obtenemos la información del contacto seleccionado.
		$this->load->model('mantenimiento_model');
		$data['estudiante'] = $this->mantenimiento_model->obtenerEtudiante($this->uri->segment(3));
 		//cargamos la vista para editar la información, pasandole dicha información.
		redirect('/Estudiantes','refresh');
		$this->load->view('editar', $data);
	}

	public function update() 
	{
 		//recogemos los datos obtenidos por POST
		$data['first_name'] = $_POST['input1'];
		$data['last_name'] = $_POST['input2'];
		$data['document_number'] = $_POST['input3'];
		$data['email'] = $_POST['input4'];
		$data['created_at'] = date('Y-m-d-G-i-s-A');
		$data['updated_at'] = date('Y-m-d-G-i-s-A');
		$data['id'] = $this->uri->segment(3);
 		//llamamos al modelo, concretamente a la función insert() para que nos haga el insert en la base de datos.
		$this->load->model('students_model');
		$this->students_model->updated($data);
 		//volvemos a visualizar la tabla
		redirect('/Estudiantes','refresh');
		$this->index();
	}
function invertir(){

	echo invertir('hola mundo');
	
}
function input(){
	$this->load->helper('text_helper');
	echo form_input('data','email','buscar', $extra = '');
}
}
?>