<?php

class Classroms extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() 
	{
 		$this->load->model('classroms_model'); //cargamos el modelo
 
 		$data['page_title'] = "Tarea 1";
 
 		//Obtener datos de la tabla 'contacto'
 		$classroms = $this->classroms_model->getData(); //llamamos a la función getData() del modelo creado anteriormente.
 
 		$data['data'] = $classroms;
		$data['JS_CSS_files'] = JS_CSS_files();
		$data['navbar'] = navbar();
 
		$heading=   array('Codigo','Nombre','Localización','Created at','Updated at','Borrar / Editar');

 		$modal= array(array('text' => 'Codigo','type' => 'text' ),
 					  array('text' => 'Nombre','type' => 'text' ),
 				 	  array('text' => 'Localización','type' => 'text' ));
		$modalRoute = 'Aulas/Agregar';
		$seccion = 'Aulas';
		$data['modal']= modal($modal,$modalRoute,$seccion);
 		$data['table']= _table($classroms,$heading,$seccion);
 
 		//load de vistas
 		$this->load->view('cruds_view', $data); //llamada a la vista, que crearemos posteriormente
	}

	public function insert() 
	{
 		//recogemos los datos obtenidos por POST
		$data['cod'] = $_POST['input1'];
		$data['name'] = $_POST['input2'];
		$data['location'] = $_POST['input3'];
		$data['created_at'] = date('Y-m-d-G-i-s-A');
		$data['updated_at'] = date('Y-m-d-G-i-s-A');
 		//llamamos al modelo, concretamente a la función insert() para que nos haga el insert en la base de datos.
		$this->load->model('classroms_model');
		$this->classroms_model->insert($data);
 		//volvemos a visualizar la tabla
		redirect('/Aulas','refresh');
		$this->index();
	}
	public function delete() 
	{
		//obtenemos el nombre
		$id = $this->uri->segment(3);
		//cargamos el modelo y llamamos a la función baja(), pasándole el nombre del registro que queremos borrar.
		$this->load->model('classroms_model');
		$this->classroms_model->delete($id);
		//mostramos la vista de nuevo.
		redirect('/Aulas','refresh');
		$this->index();
	}
	public function updated() 
	{
		//cargamos el modelo y obtenemos la información del contacto seleccionado.
		$this->load->model('mantenimiento_model');
		$data['estudiante'] = $this->mantenimiento_model->obtenerEtudiante($this->uri->segment(3));
		//cargamos la vista para editar la información, pasandole dicha información.
		redirect('/Aulas','refresh');
		$this->load->view('editar', $data);
	}

	public function update() {
		//recogemos los datos obtenidos por POST
		$data['cod'] = $_POST['input1'];
		$data['name'] = $_POST['input2'];
		$data['location'] = $_POST['input3'];
		$data['updated_at'] = date('Y-m-d-G-i-s-A');
		$data['id'] = $this->uri->segment(3);
		//llamamos al modelo, concretamente a la función insert() para que nos haga el insert en la base de datos.
		$this->load->model('classroms_model');
		$this->classroms_model->updated($data);
		//volvemos a visualizar la tabla
		redirect('/Aulas','refresh');
		$this->index();
	}
}
?>