<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = "login";
$route['404_override'] = '';
$route['Estudiantes']='students';
$route['Estudiantes/editar/(:num)']='students/update/$1';
$route['Estudiantes/borrar/(:num)']='students/delete/$1';
$route['Estudiantes/Agregar']='students/insert';
$route['Profesores']='professors';
$route['Profesores/borrar/(:num)']='professors/delete/$1';
$route['Profesores/editar/(:num)']='professors/update/$1';
$route['Profesores/Agregar']='professors/insert';
$route['Aulas']='classroms';
$route['Aulas/borrar/(:num)']='classroms/delete/$1';
$route['Aulas/editar/(:num)']='classroms/update/$1';
$route['Aulas/Agregar']='classroms/insert';
$route['Cursos']='courses';
$route['Cursos/borrar/(:num)']='courses/delete/$1';
$route['Cursos/editar/(:num)']='courses/update/$1';
$route['Cursos/Agregar']='courses/insert';
$route['Grupos']='groups';
$route['Grupos/borrar/(:num)']='groups/delete/$1';
$route['Grupos/Agregar']='groups/insert';
$route['Grupos/editar/(:num)']='groups/update/$1';





/* End of file routes.php */
/* Location: ./application/config/routes.php */