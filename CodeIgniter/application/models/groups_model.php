<?php
class Groups_model extends CI_Model {

	public function getData() {
		 $text =("select group.id,course.name,group.quarter,professor.first_name,group.group_number ,group.created_at ,group.updated_at ,group.enabled
		 	 ,professor.last_name from course, professor,`group` where course.id = group.course_id
 				   and professor.id = group.professor_id");
		$data = $this->db->query($text);

 	return $data->result(); //devolvemos el resultado de lanzar la query.
	}
	public function insert($data) 
	{
		$this->db->set('course_id', $data['course_id']);
		$this->db->set('quarter', $data['quarter']);
		$this->db->set('professor_id', $data['professor_id']);
		$this->db->set('group_number', $data['group_number']);
		$this->db->set('updated_at', $data['updated_at']);
		$this->db->insert('group');
	}
	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete('group');
	}
	public function updated($data) 
	{
		$this->db->set('course_id', $data['course_id']);
		$this->db->set('quarter', $data['quarter']);
		$this->db->set('professor_id', $data['professor_id']);
		$this->db->set('group_number', $data['group_number']);
		$this->db->set('updated_at', $data['updated_at']);
		$this->db->where('id', $data['id']);
		$this->db->update('group');
	}
	public function student_id($id) 
	{
		$this->db->select('first_name, last_name, email, username, password,id');
		$this->db->from('group');
		$this->db->where('id = ' . $id);
		$contacto = $this->db->get();
		return $contacto->result();
	}
}
?>