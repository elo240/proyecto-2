<?php
class Courses_model extends CI_Model {

	public function getData() 
	{
 		$data = $this->db->get('course'); //obtenemos la tabla 'contacto'. db->get('nombre_tabla') equivale a SELECT * FROM nombre_tabla.
 
 		return $data->result(); //devolvemos el resultado de lanzar la query.
	}	
	public function insert($data) 
	{
		$this->db->set('code', $data['cod']);
		$this->db->set('name', $data['name']);
		$this->db->set('description', $data['description']);
		$this->db->set('created_at', $data['created_at']);
		$this->db->set('updated_at', $data['updated_at']);
		$this->db->insert('course');
	}
	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete('course');
	}
	public function updated($data) 
	{
		$this->db->set('code', $data['cod']);
		$this->db->set('name', $data['name']);
		$this->db->set('description', $data['description']);
		$this->db->set('updated_at', $data['updated_at']);
		$this->db->where('id', $data['id']);
		$this->db->update('course');
	}
	public function student_id($id) 
	{
		$this->db->select('first_name, last_name, email, username, password,id');
		$this->db->from('course');
		$this->db->where('id = ' . $id);
		$contacto = $this->db->get();
		return $contacto->result();
	}
}
?>