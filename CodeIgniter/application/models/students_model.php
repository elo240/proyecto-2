<?php
class Students_model extends CI_Model {

	public function getData() 
	{
		$usuarios = $this->db->get('student'); //obtenemos la tabla 'contacto'. db->get('nombre_tabla') equivale a SELECT * FROM nombre_tabla.
 
 		return $usuarios->result(); //devolvemos el resultado de lanzar la query.
	}
	public function insert($data) 
	{
		$this->db->set('first_name', $data['first_name']);
		$this->db->set('last_name', $data['last_name']);
		$this->db->set('document_number' , $data['document_number']);
		$this->db->set('email', $data['email']);
		$this->db->set('created_at', $data['created_at']);
		$this->db->set('updated_at', $data['updated_at']);
		$this->db->insert('student');
	}
	public function delete($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete('student');
	}
	public function updated($data) 
	{
		$this->db->set('first_name', $data['first_name']);
		$this->db->set('last_name', $data['last_name']);
		$this->db->set('email', $data['email']);
		$this->db->set('updated_at', $data['updated_at']);
		$this->db->where('id', $data['id']);
		$this->db->update('student');
	}
	public function student_id($id) 
	{
		$this->db->select('first_name, last_name, email, username, password,id');
		$this->db->from('student');
		$this->db->where('id = ' . $id);
		$contacto = $this->db->get();
		return $contacto->result();
	}
}
?>