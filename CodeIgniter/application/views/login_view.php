<!DOCTYPE html>
<html>
<head>
	<title>Activos UTN</title>
	<?php foreach ($JS_CSS_files as $u):?>
		<?php print $u; ?>
	<?php endforeach;?>

</head>
<body>
	<div class="container">
		<form  action = "<?php base_url(); ?>Estudiantes" class="form-horizontal" role="form">
  			<div class="form-group">
    			<label for="inputEmail3" class="col-sm-2 control-label">Usuario</label>
    			<div class="col-sm-10">
      				<input type="text" class="form-control" id="inputEmail3" placeholder="Usuario"  required>
    			</div>
 			</div>
 			<div class="form-group">
   				<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    			<div class="col-sm-10">
     				<input type="password" class="form-control" id="inputPassword3" placeholder="Password" required>
    			</div>
  			</div>
  			<div class="form-group">
    			<div class="col-sm-offset-2 col-sm-10">
      				<div class="checkbox">
        				<label>
          					<input type="checkbox"> Remember me
        				</label>
     				</div>
    			</div>
  			</div>
  			<div class="form-group">
   				<div class="col-sm-offset-2 col-sm-10">
      				<button type="submit" class="btn btn-default">Sign in</button>
    			</div>
 			</div>
 		</form>  
	</div>
</body>
</html>